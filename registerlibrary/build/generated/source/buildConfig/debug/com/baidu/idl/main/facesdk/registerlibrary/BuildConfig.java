/**
 * Automatically generated file. DO NOT MODIFY
 */
package com.baidu.idl.main.facesdk.registerlibrary;

public final class BuildConfig {
  public static final boolean DEBUG = Boolean.parseBoolean("true");
  public static final String LIBRARY_PACKAGE_NAME = "com.baidu.idl.main.facesdk.registerlibrary";
  public static final String BUILD_TYPE = "debug";
}
