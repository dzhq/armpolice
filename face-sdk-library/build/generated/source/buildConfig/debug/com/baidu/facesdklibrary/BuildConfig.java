/**
 * Automatically generated file. DO NOT MODIFY
 */
package com.baidu.facesdklibrary;

public final class BuildConfig {
  public static final boolean DEBUG = Boolean.parseBoolean("true");
  public static final String LIBRARY_PACKAGE_NAME = "com.baidu.facesdklibrary";
  public static final String BUILD_TYPE = "debug";
  // Field from build type: debug
  public static final boolean USE_AIKL = true;
}
