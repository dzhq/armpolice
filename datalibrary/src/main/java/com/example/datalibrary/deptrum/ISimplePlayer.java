package com.example.datalibrary.deptrum;

public interface ISimplePlayer {

    public void onPlayStart();

    public void onReceiveState(int state);
}
