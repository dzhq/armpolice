package com.xnjz.armedpolice;

import android.app.Application;

import com.tencent.mmkv.MMKV;

public class MyApp extends Application {

    private static MyApp sInstance;
    @Override
    public void onCreate() {
        super.onCreate();
        sInstance = this;
        MMKV.initialize(this);
    }


    public static MyApp getInstance() {
        if (sInstance == null) {
            throw new NullPointerException("please inherit BaseApplication or call setApplication.");
        }
        return sInstance;
    }
}
