package com.xnjz.armedpolice.http;

public interface GetDataCallBack {

    <T> void onSuccess(T data);

    void onFail(String errorMessage);
}
