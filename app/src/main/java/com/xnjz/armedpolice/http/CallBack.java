package com.xnjz.armedpolice.http;

public interface CallBack {

    void onSuccess(String data);

    void onFail(String errorMessage);
}
