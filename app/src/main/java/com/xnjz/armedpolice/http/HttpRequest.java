package com.xnjz.armedpolice.http;

import com.blankj.utilcode.util.GsonUtils;
import com.blankj.utilcode.util.NetworkUtils;
import com.blankj.utilcode.util.ToastUtils;
import com.xnjz.armedpolice.MyApp;
import com.xnjz.armedpolice.R;
import com.zhy.http.okhttp.OkHttpUtils;
import com.zhy.http.okhttp.callback.StringCallback;

import java.util.HashMap;

import okhttp3.Call;

public class HttpRequest {

    private HttpRequest() {
    }

    private static HttpRequest httpRequest = null;

    public static synchronized HttpRequest getInstance() {
        if (httpRequest == null) {
            httpRequest = new HttpRequest();
        }
        return httpRequest;
    }

    /**
     * Post 请求
     *
     * @param requestUrl
     * @param map
     */


    public void post(String requestUrl, HashMap<String, String> map, CallBack callBack) {
        post(requestUrl, GsonUtils.toJson(map), false, callBack);
    }

    public void post(String requestUrl, String json, CallBack callBack) {
        post(requestUrl, json, false, callBack);
    }

    public void post(String requestUrl, String json, boolean noShowErrorMsg, CallBack callBack) {
        if (NetworkUtils.isAvailable()) {
            try {
                OkHttpUtils.postString()//
                        .url(requestUrl)
                        .content(json)
                        .tag(this)
                        .build()//
                        .execute(new StringCallback() {

                            @Override
                            public void onError(okhttp3.Call call, Exception e, int id) {

                            }

                            @Override
                            public void onResponse(String response, int id) {

                            }
                        });
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {
            ToastUtils.showShort(MyApp.getInstance().getResources().getString(R.string.error_please_check_network));
        }
    }


    /**
     * Put 请求
     *
     * @param requestUrl
     * @param callBack
     */
    public void put(String requestUrl, CallBack callBack) {
        put(requestUrl, null, false, callBack);
    }

    public void put(String requestUrl, HashMap<String, String> map, CallBack callBack) {
        put(requestUrl, map, false, callBack);
    }

    public void put(String requestUrl, HashMap<String, String> map, boolean noShowErrorMsg, CallBack callBack) {
        if (NetworkUtils.isAvailable()) {
            try {
                OkHttpUtils.put()//
                        .url(requestUrl)
                        .tag(this)
                        .build()
                        .execute(new StringCallback() {

                            @Override
                            public void onError(Call call, Exception e, int id) {

                            }

                            @Override
                            public void onResponse(String response, int id) {

                            }
                        });
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {
            ToastUtils.showShort(MyApp.getInstance().getResources().getString(R.string.error_please_check_network));
        }
    }


    /**
     * Post 请求
     *
     * @param requestUrl
     * @param callBack
     */

    public void get(String requestUrl, CallBack callBack) {
        get(requestUrl, null, false, callBack);
    }

    public void get(String requestUrl, HashMap<String, String> map, CallBack callBack) {
        get(requestUrl, map, false, callBack);
    }

    public void get(String requestUrl, HashMap<String, String> map, boolean noShowErrorMsg, CallBack callBack) {
        if (NetworkUtils.isAvailable()) {
            try {
                OkHttpUtils.get()//
                        .url(requestUrl)
                        .params(map)
                        .tag(this)
                        .build()//
                        .execute(new StringCallback() {
                            @Override
                            public void onError(Call call, Exception e, int id) {
                                if (!noShowErrorMsg) {
                                    ToastUtils.showShort(e.getMessage());
                                }
                                callBack.onFail(e.getMessage());
                            }

                            @Override
                            public void onResponse(String response, int id) {

                            }
                        });
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {
            ToastUtils.showShort(MyApp.getInstance().getResources().getString(R.string.error_please_check_network));
        }
    }



    private static <T> void getA(Class<T> approveRecordItemBeanClass) {


    }


    public <T> void getResponse(String requestUrl, HashMap<String, String> map,
                                boolean noShowErrorMsg, Class<T> commonBean, GetDataCallBack callBack) {
        if (NetworkUtils.isAvailable()) {
            try {
                OkHttpUtils.get()//
                        .url(requestUrl)
                        .params(map)
                        .tag(this)
                        .build()//
                        .execute(new ResponseCallBack<T>() {
                            @Override
                            public void onError(Call call, Exception e, int id) {

                            }

                            @Override
                            public void onResponse(CommonDataBean<T> response, int id) {
                                if (response.isSuccess()) {
                                    callBack.onSuccess(response.getData());
                                } else {
                                    callBack.onFail(response.getMsg());
                                }
                            }

                        });
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {
            ToastUtils.showShort(MyApp.getInstance().getResources().getString(R.string.error_please_check_network));
        }
    }


}
