package com.xnjz.armedpolice.http;

import com.blankj.utilcode.util.GsonUtils;
import com.zhy.http.okhttp.callback.Callback;

import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;

import okhttp3.Response;

public abstract class ResponseCallBack<T> extends Callback<CommonDataBean<T>> {


    @Override
    public CommonDataBean<T> parseNetworkResponse(Response response, int id) throws Exception {
        try {
            String res = response.body().string();
            Class<T> tClass = (Class<T>) ((ParameterizedType) getClass().getGenericSuperclass()).getActualTypeArguments()[0]; // 根据当前类获取泛型的Type
            Type ty = new ParameterizedTypeImpl(CommonDataBean.class, new Class[]{tClass}); // 传泛型的Type和我们想要的外层类的Type来组装我们想要的类型
            return GsonUtils.fromJson(res, ty);
        } catch (Exception e) {
            onError(null, e, id);
            return null;
        }
    }

    public class ParameterizedTypeImpl implements ParameterizedType {
        private final Class raw;
        private final Type[] args;

        public ParameterizedTypeImpl(Class raw, Type[] args) {
            this.raw = raw;
            this.args = args != null ? args : new Type[0];
        }

        @Override
        public Type[] getActualTypeArguments() {
            return args;
        }

        @Override
        public Type getRawType() {
            return raw;
        }

        @Override
        public Type getOwnerType() {
            return null;
        }
    }

}
