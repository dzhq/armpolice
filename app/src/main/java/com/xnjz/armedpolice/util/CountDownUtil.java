package com.xnjz.armedpolice.util;

import android.os.CountDownTimer;

public class CountDownUtil {


    static CountDownTimer countDownTimer = null;

    private static long timeLong = 0;


    public long getTimeLong() {
        return timeLong;
    }

    public static void countDownTimer() {
        //这里所有的参数都是毫秒，使用秒需要 time乘以1000
        //long millisInFuture 倒计时总时间
        //long countDownInterval 间隔多久倒计时一次
        //以下参数是 一共60秒，每19秒倒计时一次
        if (countDownTimer == null) {
            countDownTimer = new CountDownTimer(Integer.MAX_VALUE * 1000L, 1000) {
                @Override
                public void onTick(long millisUntilFinished) {
                    timeLong++;

//                    binding.tvWorkingTime.updateShow(timeLong * 1000);
                }

                @Override
                public void onFinish() {
                    //onFinish() 倒计时结束

                }
            };
            countDownTimer.start();
        }
    }


    public static void cancelCountDownTimer() {
        if (countDownTimer != null) {
            countDownTimer.cancel();
        }
    }
}
