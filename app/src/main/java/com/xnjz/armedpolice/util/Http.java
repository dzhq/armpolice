package com.xnjz.armedpolice.util;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.FutureTask;

public class Http {

//    public static final String SERVER="http://192.168.5.48";
    public static final String SERVER="https://api.chato.cn/chato/api-public/domains/jp1637w62nmrmo8e/chat";

    public static String get(String uri) {
        Callable<String> call = new Callable<String>() {
            @Override
            public String call() throws Exception {
                StringBuilder sb = new StringBuilder();
                try {
                    // 1. URL
                    URL url = new URL(uri);
                    // 2. HttpURLConnection
                    HttpURLConnection conn = (HttpURLConnection) url.openConnection();
                    // 3. set(GET)
                    conn.setRequestMethod("GET");
                    // 4. getInputStream
                    InputStream is = conn.getInputStream();
                    // 5. 解析is，获取responseText，这里用缓冲字符流
                    BufferedReader reader = new BufferedReader(new InputStreamReader(is));
                    String line = null;
                    while ((line = reader.readLine()) != null) {
                        sb.append(line);
                    }
                } catch (ProtocolException e) {
                    throw new RuntimeException(e);
                } catch (MalformedURLException e) {
                    throw new RuntimeException(e);
                } catch (IOException e) {
                    throw new RuntimeException(e);
                }
                // 获取响应文本
                return sb.toString();
            }
        };
        FutureTask<String> task = new FutureTask<String>(call);
        Thread t = new Thread(task);
        t.start();
        try {
            return task.get();
        } catch (InterruptedException | ExecutionException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
            return "";
        }
    }

    public static String post(String uri,String json) {
        Callable<String> call = new Callable<String>() {
            @Override
            public String call() throws Exception {
                StringBuilder sb = new StringBuilder();
                try {
                    // 1. URL
                    URL url = new URL(uri);
                    // 2. HttpURLConnection
                    HttpURLConnection conn = (HttpURLConnection) url.openConnection();
                    // 3. POST
                    conn.setRequestMethod("POST");
                    // 4. Content-Type,这里是固定写法，发送内容的类型
                    conn.setRequestProperty("Content-Type", "application/json");
                    // 5. output，这里要记得开启输出流，将自己要添加的参数用这个输出流写进去，传给服务端，这是socket的基本结构
                    conn.setDoOutput(true);
                    OutputStream os = conn.getOutputStream();
//                    String param = "";// 一定要记得将自己的参数转换为字节，编码格式是utf-8
                    os.write(json.getBytes("utf-8"));
                    os.flush();
                    // 6. is
                    InputStream is = conn.getInputStream();
                    // 7. 解析is，获取responseText
                    BufferedReader reader = new BufferedReader(new InputStreamReader(is));

                    String line = null;
                    while ((line = reader.readLine()) != null) {
                        sb.append(line);
                    }
                } catch (Throwable e) {
                    return "{\"data\":{\"content\":\"访问接口失败\"}}";
                }
                // 获取响应文本
                return sb.toString();
            }
        };
        FutureTask<String> task = new FutureTask<String>(call);
        Thread t = new Thread(task);
        t.start();
        try {
            return task.get();
        } catch (Throwable e) {
            return "{\"data\":{\"content\":\"访问接口失败\"}}";
        }
    }
}
