package com.xnjz.armedpolice.util;

import android.content.Context;
import android.content.res.AssetFileDescriptor;
import android.media.MediaPlayer;
import android.media.RingtoneManager;
import android.net.Uri;

import com.blankj.utilcode.util.LogUtils;
import com.xnjz.armedpolice.R;

import java.io.IOException;

public class SoundUtil {

    private MediaPlayer mMediaPlayer;
    private MediaPlayer player;

//    /**
//     * 开启手机系统自带铃声
//     */
//    public void startAlarm(Context context) {
//        mMediaPlayer = MediaPlayer.create(context, getSystemDefultRingtoneUri(context));
//        mMediaPlayer.setLooping(true);
//        try {
//            mMediaPlayer.prepare();
//        } catch (IllegalStateException e) {
//            e.printStackTrace();
//        } catch (IOException e) {
//            e.printStackTrace();
//        }
//        mMediaPlayer.start();
//    }
//
//    public void stopAlarm() {
//        mMediaPlayer.stop();
//    }

//    /**
//     * 获取系统自带铃声的uri
//     *
//     * @return RingtoneManager.getActualDefaultRingtoneUri(this,
//     *RingtoneManager.TYPE_RINGTONE)
//     */
//    public Uri getSystemDefultRingtoneUri(Context context) {
//        return RingtoneManager.getActualDefaultRingtoneUri(context,
//                RingtoneManager.TYPE_RINGTONE);
//    }


    /**
     * 播放本地音频
     * <p>
     * * @param mContext
     * <p>
     */

    public void playFromRawFile(Context mContext, int soundFile, PlayerCallBack playerCallBack) {
        LogUtils.d("rogerzhagn, playFromRawFile ");
        try {
            if (player != null) {
                if (player.isPlaying()) {
                    player.stop();
                }
            }
            player = new MediaPlayer();
            AssetFileDescriptor file = mContext.getResources().openRawResourceFd(soundFile);
            try {
                player.setDataSource(file.getFileDescriptor(), file.getStartOffset(), file.getLength());
                player.setOnCompletionListener(mediaPlayer -> {
                    if (playerCallBack != null) {
                        playerCallBack.playComplete();
                    }
                });
                file.close();
                if (!player.isPlaying()) {
                    player.prepare();
                    player.start();
                    player.setLooping(false);
                }
            } catch (IOException e) {
                player = null;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * * 停止播放本地音频
     */
    public void stopPlayFromRawFile() {
        if (player != null) {
            player.stop();
            player.release();
        }
    }

    private PlayerCallBack playerCallBack;

    public interface PlayerCallBack {
        void playComplete();
    }
}
