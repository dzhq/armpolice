package com.xnjz.armedpolice;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import com.baidu.speech.EventManager;
import com.baidu.speech.EventManagerFactory;
import com.baidu.speech.asr.SpeechConstant;
import com.xnjz.armedpolice.base.BaseActivity;
import com.xnjz.armedpolice.databinding.ActivityStartBinding;
import com.xnjz.armedpolice.ui.WorkingActivity;
import com.xnjz.armedpolice.util.Http;
import com.xnjz.armedpolice.util.SpeechUtils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;

public class StartActivity extends BaseActivity {

    private Context mContext;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(getLayoutBinding());
        mContext = this;
        initLicense();
        initai();
    }
    private ActivityStartBinding binding;

    @Override
    public View getLayoutBinding() {
        binding = ActivityStartBinding.inflate(getLayoutInflater());
        return binding.getRoot();
    }

    private void initLicense() {

        TimerTask task = new TimerTask() {
            @Override
            public void run() {
                /**
                 *要执行的操作
                 */
                startActivity(new Intent(mContext, WorkingActivity.class));
                finish();
            }
        };
        Timer timer = new Timer();
        timer.schedule(task, 1000);

        // todo  这个是人脸识别的代码，方便调试语音先注释掉

//        FaceSDKManager.getInstance().init(mContext, new SdkInitListener() {
//            @Override
//            public void initStart() {
//
//            }
//
//            public void initLicenseSuccess() {
//
//                TimerTask task = new TimerTask() {
//                    @Override
//                    public void run() {
//                        /**
//                         *要执行的操作
//                         */
//                        startActivity(new Intent(mContext, WorkingActivity.class));
//                        finish();
//                    }
//                };
//                Timer timer = new Timer();
//                timer.schedule(task, 2000);
//            }
//
//            @Override
//            public void initLicenseFail(int errorCode, String msg) {
//                TimerTask task = new TimerTask() {
//                    @Override
//                    public void run() {
//
//
//                        ToastUtils.showShort(errorCode + " = code 初始化错误, msg = " + msg);
//                        /**
//                         *要执行的操作
//                         */
////                        startActivity(new Intent(mContext, ActivitionActivity.class));
////                        finish();
//                    }
//                };
//                Timer timer = new Timer();
//                timer.schedule(task, 2000);
//            }
//
//            @Override
//            public void initModelSuccess() {
//            }
//
//            @Override
//            public void initModelFail(int errorCode, String msg) {
//
//            }
//        });
    }


    private final static String TAG="APP";
    EventManager wakeup;//唤醒
    EventManager asr;//识别
    private boolean lock = true; //为false时不再启动语音唤醒
    private SpeechUtils speech;

    private void initai(){
        initPermission();//6.0才用动态权限
        speech= SpeechUtils.getInstance(this);
        wakeup = EventManagerFactory.create(this, "wp");
        wakeup.registerListener((name,params,data,offset,length)->{//唤醒
            if(name.equals("wp.data")){
                try {
                    JSONObject json = new JSONObject(params);
                    int errorCode = (int)json.get("errorCode");
                    if(errorCode == 0){
                        //Mp3Util.reset();
                        Log.d(TAG,"唤醒成功:"+json.get("word"));
                        speech.stop();
                        speech.speak("您好");
                        new Handler().postDelayed(()->{
                            startyy();//语音识别
                        }, 1500);//1.5秒后执行Runnable中的run方法
                    } else {
                        Log.d(TAG,"唤醒失败");
                    }
                }catch (JSONException e){
                    Log.d(TAG,"唤醒err"+e.getMessage());
                }

            } else if("wp.exit".equals(name)){
                Log.d(TAG,"唤醒停止");
            }
        });
        asr = EventManagerFactory.create(this, "asr");
        asr.registerListener((String name, String params, byte [] data, int offset, int length)->{
            if (name.equals(SpeechConstant.CALLBACK_EVENT_ASR_PARTIAL)) {
                if (params == null || params.isEmpty()) {
                    return;
                }
                if (params.contains("\"final_result\""))  {

                    try {
                        JSONObject json = new JSONObject(params);
                        Log.d(TAG,"监听说话:"+json);
                        JSONArray sps= json.getJSONArray("results_recognition");
                        String anw=sps.getString(0);
                        new Thread() {
                            public void run() {//这儿是耗时操作，完成之后更新UI；
                                speech.speak("让我想想,请稍等");
                                String jx= Http.post(Http.SERVER,"{\"p\":\""+anw+"\",\"uid\":\"1\",\"token\":\"af4d6a65a1b0602c\"}");
                                String res="网络接口出现问题";
                                try {
                                    res=new JSONObject(jx).getJSONObject("data").getString("content");
                                }catch (Exception e) {
                                }
                                String res1=res;
                                runOnUiThread(()->{
                                    stopWakeUp();
                                    startWakeUp();
                                    speech.speak(res1);
                                });
                            }
                        }.start();
//                        start();
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }


                }
            }
        });
        Toast.makeText(StartActivity.this,"好了",Toast.LENGTH_LONG).show();
        new Handler().postDelayed(()->{
            startWakeUp();
        }, 1000);//2秒后执行Runnable中的run方法
    }

    private void startyy() {
        if(lock==false){
            return;
        }
        Map<String, Object> params = new LinkedHashMap<String, Object>();
        params.put(SpeechConstant.APP_ID, "44243247");
        params.put(SpeechConstant.APP_KEY, "yjgvGC3H6dAi8HWw6qhV2t49");
        params.put(SpeechConstant.SECRET, "ZmHBDWgCGBykX5u00WDaXAlI7toRBK5z");
        params.put(SpeechConstant.ACCEPT_AUDIO_VOLUME, false);//设置识别参数
        asr.send(SpeechConstant.ASR_START,new JSONObject(params).toString(), null, 0, 0);
    }

    private void startWakeUp() {
        Map<String, Object> params = new LinkedHashMap<String, Object>();
        params.put(SpeechConstant.APP_ID, "44243247");
        params.put(SpeechConstant.APP_KEY, "yjgvGC3H6dAi8HWw6qhV2t49");
        params.put(SpeechConstant.SECRET, "ZmHBDWgCGBykX5u00WDaXAlI7toRBK5z");
        params.put(SpeechConstant.ACCEPT_AUDIO_VOLUME, false);
        params.put(SpeechConstant.WP_WORDS_FILE, "assets://WakeUp.bin");
        String json = new JSONObject(params).toString();
        wakeup.send(SpeechConstant.WAKEUP_START, json, null, 0, 0);
        Log.d(TAG,"唤醒参数：" + json);
    }

    private void stopWakeUp() {
        //lock = false; //不再识别
        wakeup.send(SpeechConstant.WAKEUP_STOP, null, null, 0, 0); //
    }

    /**
     * android 6.0 以上需要动态申请权限
     */
    private void initPermission() {
        String permissions[] = {
                Manifest.permission.RECORD_AUDIO,
                Manifest.permission.READ_PHONE_STATE,
                Manifest.permission.ACCESS_NETWORK_STATE,
                Manifest.permission.READ_EXTERNAL_STORAGE,
                Manifest.permission.WRITE_EXTERNAL_STORAGE,
                Manifest.permission.MODIFY_AUDIO_SETTINGS,
                Manifest.permission.ACCESS_WIFI_STATE,
                Manifest.permission.WAKE_LOCK,
                Manifest.permission.RECEIVE_BOOT_COMPLETED,
                Manifest.permission.INTERNET
        };
        ArrayList<String> toApplyList = new ArrayList<String>();
        for (String perm :permissions){
            if (PackageManager.PERMISSION_GRANTED != ContextCompat.checkSelfPermission(this, perm)) {
                toApplyList.add(perm);
                //进入到这里代表没有权限.

            }
        }
        String tmpList[] = new String[toApplyList.size()];
        if (!toApplyList.isEmpty()){
            ActivityCompat.requestPermissions(this, toApplyList.toArray(tmpList), 123);
        }
    }
    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        // 此处为android 6.0以上动态授权的回调，用户自行实现。
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

    }
}
