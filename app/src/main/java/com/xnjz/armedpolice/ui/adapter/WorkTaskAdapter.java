package com.xnjz.armedpolice.ui.adapter;

import android.text.TextUtils;

import androidx.annotation.NonNull;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.viewholder.BaseViewHolder;
import com.xnjz.armedpolice.R;
import com.xnjz.armedpolice.ui.bean.WorkTaskBean;

public class WorkTaskAdapter extends BaseQuickAdapter<WorkTaskBean, BaseViewHolder> {

    public WorkTaskAdapter() {
        super(R.layout.adapter_work_task);
    }

    @Override
    protected void convert(@NonNull BaseViewHolder holder, WorkTaskBean workTaskBean) {

        holder.setText(R.id.tv_time, workTaskBean.getTimeStr());
        holder.setText(R.id.tv_state, workTaskBean.isPass() ? "已完成" : "未完成");
        holder.setText(R.id.tv_content, getContent(workTaskBean.getContent()));

        holder.setBackgroundColor(R.id.tv_state, workTaskBean.isPass() ?
                getContext().getResources().getColor(R.color.yellow) :
                getContext().getResources().getColor(R.color.yellow_dark));
    }

    private String getContent(String content) {
        if (TextUtils.isEmpty(content)) {
            return "";
        } else {
            return "友情提示:" + content;
        }
    }
}
