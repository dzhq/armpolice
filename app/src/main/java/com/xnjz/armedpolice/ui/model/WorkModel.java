package com.xnjz.armedpolice.ui.model;

import android.content.Context;
import android.text.TextUtils;

import com.xnjz.armedpolice.R;
import com.xnjz.armedpolice.ui.bean.UserInfoBean;
import com.xnjz.armedpolice.ui.bean.WorkTaskBean;
import com.xnjz.armedpolice.util.SoundUtil;

import java.util.ArrayList;
import java.util.List;

public class WorkModel {


    public static final int VOICE_WELCOME = 1;
    public static final int VOICE_BULLET_BOX = 2;

    public static final String WelcomeStr = "同志，您已进入站位，请提高警惕，认真履行职责。你的职责是1.了解执勤目标有关情况和周边数社情，热悉哨位的任务、执勤设施、警成区域地形以及友邻哦位\n2.熟愿执勤方棠和有关制度脱定，牢记并正确使用口令以及信(记) 号规定\n3熟愿有关证件以及使用规定\n4按照规定着袋，携带警减，武器以及执勤用品";
//    public static final int VOICE_WELCOME = 1;

    public static void playWelcome(Context mContext, String userName, SoundUtil soundUtil, int type, SoundUtil.PlayerCallBack callBack) {
        if (TextUtils.equals(userName, "付伟")) {
            soundUtil.playFromRawFile(mContext, R.raw.fuwei_name, () -> {
                playVoice(mContext, soundUtil, type, callBack);
            });
        } else if (TextUtils.equals(userName, "张鑫")) {
            soundUtil.playFromRawFile(mContext, R.raw.fuwei_name, () -> {
                playVoice(mContext, soundUtil, type, callBack);
            });
        } else if (TextUtils.equals(userName, "刘德华")) {
            soundUtil.playFromRawFile(mContext, R.raw.fuwei_name, () -> {
                playVoice(mContext, soundUtil, type, callBack);
            });
        }
    }

    private static void playVoice(Context mContext, SoundUtil soundUtil, int type, SoundUtil.PlayerCallBack callBack) {
        switch (type) {
            case VOICE_WELCOME:
                playWelcome(mContext, soundUtil, callBack);
                break;
            case VOICE_BULLET_BOX:
                playBulletBoxTwo(mContext, soundUtil, callBack);
                break;
        }


    }


    /**
     * 播放欢迎语
     *
     * @param context
     * @param soundUtil
     */
    public static void playWelcome(Context context, SoundUtil soundUtil, SoundUtil.PlayerCallBack callBack) {
        soundUtil.playFromRawFile(context, R.raw.start_work_one, callBack);
    }


    /**
     * 播放子弹箱子
     *
     * @param context
     * @param soundUtil
     */
    public static void playBulletBoxTwo(Context context, SoundUtil soundUtil, SoundUtil.PlayerCallBack callBack) {
        soundUtil.playFromRawFile(context, R.raw.bullet_box_two, callBack);
    }


    public List<UserInfoBean> getUserInfo() {

        // 刘德华
        List<UserInfoBean> userInfoBeanList = new ArrayList<>();
        UserInfoBean andyLiuBean = new UserInfoBean();
        andyLiuBean.setId("1");
        andyLiuBean.setName("刘德华");
        andyLiuBean.setSex("男");
        andyLiuBean.setNation("汉");
        andyLiuBean.setBirthday("1980-01-02");
        andyLiuBean.setAddressStr("天津市和平区合生财富广场");
        andyLiuBean.setIdCard("110102198001021980");
        andyLiuBean.setCardUnit("天津市公安局");
        andyLiuBean.setExpiredDate("2027-11-10");

        // 付伟
        UserInfoBean fuWeiBean = new UserInfoBean();
        fuWeiBean.setId("2");
        fuWeiBean.setName("付伟");
        fuWeiBean.setSex("男");
        fuWeiBean.setNation("汉");
        fuWeiBean.setBirthday("1990-02-03");
        fuWeiBean.setAddressStr("天津市西青区星企中心");
        fuWeiBean.setIdCard("110102199002031990");
        fuWeiBean.setCardUnit("天津市西青区公安局");
        fuWeiBean.setExpiredDate("2026-10-09");

        // 张鑫
        UserInfoBean zhangXinBean = new UserInfoBean();
        zhangXinBean.setId("3");
        zhangXinBean.setName("张鑫");
        zhangXinBean.setSex("男");
        zhangXinBean.setNation("汉");
        zhangXinBean.setBirthday("2000-03-04");
        zhangXinBean.setAddressStr("天津市西青区星企中心");
        zhangXinBean.setIdCard("110102200003042001");
        zhangXinBean.setCardUnit("天津市武清区公安局");
        zhangXinBean.setExpiredDate("2028-12-11");

        userInfoBeanList.add(andyLiuBean);
        userInfoBeanList.add(fuWeiBean);
        userInfoBeanList.add(zhangXinBean);
        return userInfoBeanList;
    }

    /**
     * 获取右侧任务
     */
    public List<WorkTaskBean> getWorkTaskList(String userName) {
        List<WorkTaskBean> workTaskList = new ArrayList<>();

        WorkTaskBean workTaskBean1 = new WorkTaskBean();
        workTaskBean1.setContent(userName + WorkModel.WelcomeStr);
        workTaskBean1.setId("1");
        workTaskBean1.setPass(false);
        workTaskBean1.setTimeStr("上午 10:00");
        workTaskList.add(workTaskBean1);

        WorkTaskBean workTaskBean = new WorkTaskBean();
        workTaskBean.setContent(userName + "同志，请观察弹箱。具体要求是:1.检查弹药质量是否完好，2.检查弹药存放位置是否便于领取");
        workTaskBean.setId("1");
        workTaskBean.setPass(false);
        workTaskBean.setTimeStr("上午 10:30");
        workTaskList.add(workTaskBean);


//        WorkTaskBean workTaskBean2 = new WorkTaskBean();
//        workTaskBean2.setContent(userName + "同志，请调取周界画面，了解掌握周边敌社情；1.查看哨位盲区有无并常，2.查看有位险点有无变化，3.查看友邻啃兵履职情况");
//        workTaskBean2.setId("2");
//        workTaskBean2.setPass(false);
//        workTaskBean2.setTimeStr("上午 10:30");
//        workTaskList.add(workTaskBean2);
        return workTaskList;
    }
}
