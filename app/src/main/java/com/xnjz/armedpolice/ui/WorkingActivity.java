package com.xnjz.armedpolice.ui;

import android.content.Intent;
import android.hardware.Camera;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Handler;
import android.os.Message;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.baidu.idl.main.facesdk.model.BDFaceSDKCommon;
import com.baidu.speech.asr.SpeechConstant;
import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.listener.OnItemClickListener;
import com.example.datalibrary.callback.CameraDataCallback;
import com.example.datalibrary.callback.FaceDetectCallBack;
import com.example.datalibrary.gatecamera.CameraPreviewManager;
import com.example.datalibrary.gl.view.GlMantleSurfacView;
import com.example.datalibrary.listener.SdkInitListener;
import com.example.datalibrary.manager.FaceSDKManager;
import com.example.datalibrary.model.BDFaceCheckConfig;
import com.example.datalibrary.model.BDFaceImageConfig;
import com.example.datalibrary.model.LivenessModel;
import com.example.datalibrary.utils.ToastUtils;
import com.xnjz.armedpolice.MyRecognizer;
import com.xnjz.armedpolice.R;
import com.xnjz.armedpolice.baidu.AuthUtil;
import com.xnjz.armedpolice.baidu.AutoCheck;
import com.xnjz.armedpolice.baidu.listener.IRecogListener;
import com.xnjz.armedpolice.baidu.listener.MessageStatusRecogListener;
import com.xnjz.armedpolice.baidu.wakeup.InFileStream;
import com.xnjz.armedpolice.baidu.wakeup.MyWakeup;
import com.xnjz.armedpolice.baidu.wakeup.listener.IWakeupListener;
import com.xnjz.armedpolice.baidu.wakeup.listener.RecogWakeupListener;
import com.xnjz.armedpolice.base.BaseActivity;
import com.xnjz.armedpolice.databinding.ActivityWorkingBinding;
import com.xnjz.armedpolice.demo.SingleBaseConfig;
import com.xnjz.armedpolice.ui.adapter.WorkTaskAdapter;
import com.xnjz.armedpolice.ui.bean.WorkTaskBean;
import com.xnjz.armedpolice.ui.model.WorkModel;
import com.xnjz.armedpolice.util.FaceUtils;
import com.xnjz.armedpolice.util.SoundUtil;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;


public class WorkingActivity extends BaseActivity implements View.OnClickListener {


    private ActivityWorkingBinding binding;
    private static final int PREFER_WIDTH = SingleBaseConfig.getBaseConfig().getRgbAndNirWidth();
    private static final int PREFER_HEIGHT = SingleBaseConfig.getBaseConfig().getRgbAndNirHeight();
    private GlMantleSurfacView glMantleSurfacView;

    private BDFaceImageConfig bdFaceImageConfig; //  摄像头逐帧数据
    private BDFaceCheckConfig bdFaceCheckConfig; // 识别阈值

    private String userName = "";
    private WorkTaskAdapter workTaskAdapter;
    private SoundUtil soundUtil;
    private WorkModel workModel;


    // 百度唤醒词
//    protected MyWakeup myWakeup;
    private MyRecognizer myRecognizer;
    private MyWakeup myWakeup;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        InFileStream.setContext(this);
        setContentView(getLayoutBinding());


        workModel = new WorkModel();

        Intent intent = getIntent();
        if (intent != null) {
            Bundle extras = intent.getExtras();
            if (extras != null) {
                userName = extras.getString("username", "");
            }
        }

        soundUtil = new SoundUtil();
        new Handler().postDelayed(() -> {
            if (!TextUtils.isEmpty(userName)) {
//                countDownTimer();
                binding.layoutTaskMessage.setVisibility(View.VISIBLE);
                binding.tvTaskMessage.setText(userName + WorkModel.WelcomeStr);
                WorkModel.playWelcome(WorkingActivity.this, userName, soundUtil, WorkModel.VOICE_WELCOME, () -> {
                    binding.layoutTaskMessage.setVisibility(View.GONE);
                });
            }
        }, 1000);

        initView();

        // todo  这个是人脸识别的代码，方便调试语音先注释掉
//        initListener();

        initBaiduVoice();

    }

    private void initBaiduVoice() {
        // 改为 SimpleWakeupListener 后，不依赖handler，但将不会在UI界面上显示
        // 基于DEMO唤醒词集成第1.1, 1.2, 1.3步骤
        IWakeupListener listener = new RecogWakeupListener(handler);
        myWakeup = new MyWakeup(this, listener);


        // 基于DEMO集成第1.1, 1.2, 1.3 步骤 初始化EventManager类并注册自定义输出事件
        // DEMO集成步骤 1.2 新建一个回调类，识别引擎会回调这个类告知重要状态和识别结果
        IRecogListener listenerRecog = new MessageStatusRecogListener(handler);
        // DEMO集成步骤 1.1 1.3 初始化：new一个IRecogListener示例 & new 一个 MyRecognizer 示例,并注册输出事件
        myRecognizer = new MyRecognizer(this, listenerRecog);

    }

    @Override
    protected void onResume() {
        super.onResume();
        // todo  这个是人脸识别的代码，方便调试语音先注释掉
//        startTestOpenDebugRegisterFunction();
        binding.layoutTaskMessage.setVisibility(View.GONE);
    }

    @Override
    protected void onPause() {
        super.onPause();

        soundUtil.stopPlayFromRawFile();
        CameraPreviewManager.getInstance().stopPreview();

        // 基于DEMO唤醒词集成第5 退出事件管理器
        if (myWakeup != null) myWakeup.release();


        if (myRecognizer != null) myRecognizer.release();
    }

    @Override
    protected void onDestroy() {

        super.onDestroy();
    }


    /**
     * 开始录音，点击“开始”按钮后调用。
     * 基于DEMO集成2.1, 2.2 设置识别参数并发送开始事件
     */
    protected void startRecog() {

        com.blankj.utilcode.util.ToastUtils.showShort("开始识别");

        // DEMO集成步骤2.1 拼接识别参数： 此处params可以打印出来，直接写到你的代码里去，最终的json一致即可。
        final Map<String, Object> params = fetchParams();
        // params 也可以根据文档此处手动修改，参数会以json的格式在界面和logcat日志中打印
        Log.i("roger", "设置的start输入参数：" + params);
        // 复制此段可以自动检测常规错误
        (new AutoCheck(getApplicationContext(), new Handler() {
            public void handleMessage(Message msg) {
                if (msg.what == 100) {
                    AutoCheck autoCheck = (AutoCheck) msg.obj;
                    synchronized (autoCheck) {
                        String message = autoCheck.obtainErrorMessage(); // autoCheck.obtainAllMessage();
//                        txtLog.append(message + "\n");


                        com.blankj.utilcode.util.ToastUtils.showShort("message   " + message);
                        ; // 可以用下面一行替代，在logcat中查看代码
                         Log.w("AutoCheckMessage", message);
                    }
                }
            }
        }, false)).checkAsr(params);

        // 这里打印出params， 填写至您自己的app中，直接调用下面这行代码即可。
        // DEMO集成步骤2.2 开始识别
        myRecognizer.start(params);
    }


    /**
     * 开始录音后，手动点击“停止”按钮。
     * SDK会识别不会再识别停止后的录音。
     * 基于DEMO集成4.1 发送停止事件 停止录音
     */
    protected void stopRecog() {

        com.blankj.utilcode.util.ToastUtils.showShort("结束识别");

        myRecognizer.stop();
    }

    /**
     * 开始录音后，手动点击“取消”按钮。
     * SDK会取消本次识别，回到原始状态。
     * 基于DEMO集成4.2 发送取消事件 取消本次识别
     */
    protected void cancelRecog() {

        myRecognizer.cancel();
    }


    // 点击“开始识别”按钮
    // 基于DEMO唤醒词集成第2.1, 2.2 发送开始事件开始唤醒
    private void start() {
        com.blankj.utilcode.util.ToastUtils.showShort("开始识别");

        Map<String, Object> params = AuthUtil.getParam();
        params.put(SpeechConstant.WP_WORDS_FILE, "assets:///WakeUp.bin");
        // "assets:///WakeUp.bin" 表示WakeUp.bin文件定义在assets目录下

        // params.put(SpeechConstant.ACCEPT_AUDIO_DATA,true);
        // params.put(SpeechConstant.IN_FILE,"res:///com/baidu/android/voicedemo/wakeup.pcm");
        // params里 "assets:///WakeUp.bin" 表示WakeUp.bin文件定义在assets目录下
        myWakeup.start(params);
    }

    // 基于DEMO唤醒词集成第4.1 发送停止事件
    protected void stop() {

        com.blankj.utilcode.util.ToastUtils.showShort("结束识别");
        if (myWakeup != null) myWakeup.stop();
    }

    private void initView() {
        binding.tvSign.setOnClickListener(this);

        binding.tvWorkHome.setOnClickListener(this);
        binding.tvWorkVideo.setOnClickListener(this);
        binding.tvWorkTips.setOnClickListener(this);
        binding.tvWorkRecord.setOnClickListener(this);
        binding.tvEquipmentManager.setOnClickListener(this);
        binding.tvPeopleManager.setOnClickListener(this);

        // 开始计时上稍时长
//        binding.tvWorkingTime.updateShow();

        binding.tvWorkingName.setText("欢迎您：" + userName);
        binding.tvCloseScreen.setOnClickListener(this);
        binding.tvWorkingTimeTitle.setOnClickListener(this);

        binding.tvWorkingNotification.requestFocus();
        workTaskAdapter = new WorkTaskAdapter();
        binding.rvWorkingTask.setLayoutManager(new LinearLayoutManager(this));
        binding.rvWorkingTask.setAdapter(workTaskAdapter);

        workTaskAdapter.setOnItemClickListener((adapter, view, position) -> {
            WorkTaskBean workTaskBean = workTaskAdapter.getData().get(position);
            if (position == 0) {
                binding.layoutTaskMessage.setVisibility(View.VISIBLE);
                binding.tvTaskMessage.setText(workTaskBean.getContent());
                WorkModel.playWelcome(WorkingActivity.this, userName, soundUtil, WorkModel.VOICE_WELCOME, () -> {
                    binding.layoutTaskMessage.setVisibility(View.GONE);
                });
            } else if (position == 1) {

                binding.layoutTaskMessage.setVisibility(View.VISIBLE);
                binding.tvTaskMessage.setText(workTaskBean.getContent());
                WorkModel.playWelcome(WorkingActivity.this, userName, soundUtil, WorkModel.VOICE_BULLET_BOX, () -> {
                    binding.layoutTaskMessage.setVisibility(View.GONE);
                });
            }
            workTaskBean.setPass(true);
            workTaskAdapter.notifyDataSetChanged();
        });


        initAdapterData();

        // 展示视频
        bdFaceCheckConfig = FaceUtils.getInstance().getBDFaceCheckConfig();
        glMantleSurfacView = findViewById(R.id.face_camera);
        glMantleSurfacView.initSurface(SingleBaseConfig.getBaseConfig().getRgbRevert(),
                SingleBaseConfig.getBaseConfig().getMirrorVideoRGB(), false);


        CameraPreviewManager.getInstance().startPreview(glMantleSurfacView,
                SingleBaseConfig.getBaseConfig().getRgbVideoDirection(),
                PREFER_WIDTH,
                PREFER_HEIGHT);
    }

    private void initAdapterData() {
        if (TextUtils.isEmpty(userName)) {
            return;
        }
        List<WorkTaskBean> workTaskList = workModel.getWorkTaskList(userName);
        workTaskAdapter.setList(workTaskList);
    }

    @Override
    public View getLayoutBinding() {
        binding = ActivityWorkingBinding.inflate(getLayoutInflater());
        return binding.getRoot();
    }


    // 开始识别
    private void startTestOpenDebugRegisterFunction() {
        CameraPreviewManager.getInstance().setCameraFacing(0);
        int[] cameraSize = CameraPreviewManager.getInstance().initCamera();

        initFaceConfig(cameraSize[1], cameraSize[0]);
        CameraPreviewManager.getInstance().setmCameraDataCallback(new CameraDataCallback() {

            @Override
            public void onGetCameraData(byte[] data, Camera camera, int width, int height) {


                bdFaceImageConfig.setData(data);
                checkData();
                glMantleSurfacView.onGlDraw(null, null, null);
            }
        });

    }

    private void initListener() {
        if (FaceSDKManager.initStatus != FaceSDKManager.SDK_MODEL_LOAD_SUCCESS) {
            FaceSDKManager.getInstance().initModel(WorkingActivity.this, FaceUtils.getInstance().getBDFaceSDKConfig(), new SdkInitListener() {
                @Override
                public void initStart() {

                }

                @Override
                public void initLicenseSuccess() {

                }

                @Override
                public void initLicenseFail(int errorCode, String msg) {

                }

                @Override
                public void initModelSuccess() {
                    FaceSDKManager.initModelSuccess = true;
//                    ToastUtils.toast(CameraActivity.this, "模型加载成功， 欢迎使用");
                }

                @Override
                public void initModelFail(int errorCode, String msg) {
                    FaceSDKManager.initModelSuccess = false;
                    ToastUtils.toast(WorkingActivity.this, "模型加载失败， 请尝试重启应用");
                }
            });
        }
    }

    // 初始化人脸图片配置
    private void initFaceConfig(int height, int width) {
        bdFaceImageConfig = new BDFaceImageConfig(height, width,
                SingleBaseConfig.getBaseConfig().getRgbDetectDirection(),
                SingleBaseConfig.getBaseConfig().getMirrorDetectRGB(),
                BDFaceSDKCommon.BDFaceImageType.BDFACE_IMAGE_TYPE_YUV_NV21);
    }

    // 比对数据
    private void checkData() {
        FaceSDKManager.getInstance().onDetectCheck(bdFaceImageConfig, null, null,
                bdFaceCheckConfig, new FaceDetectCallBack() {
                    @Override
                    public void onFaceDetectCallback(LivenessModel livenessModel) {
                        // 识别结果返回 包含活体得分，质量得分， 识别人员信息， 单接口与整体耗时

                    }

                    @Override
                    public void onTip(int code, String msg) {

                    }

                    @Override
                    public void onFaceDetectDarwCallback(LivenessModel livenessModel) {

                    }
                });
    }


    @Override
    public void onClick(View view) {
        // 点击时，取消播放
        soundUtil.stopPlayFromRawFile();
        binding.layoutTaskMessage.setVisibility(View.GONE);

        switch (view.getId()) {
            case R.id.tv_sign:
                startActivity(new Intent(WorkingActivity.this, CameraActivity.class));
                break;

            case R.id.tv_work_home:
                clickItem();

                binding.tvWorkHome.setBackgroundDrawable(getResources().getDrawable(R.drawable.shape_yellow_light_radius5));
                break;

            case R.id.tv_work_video:
                clickItem();

                binding.tvWorkVideo.setBackgroundDrawable(getResources().getDrawable(R.drawable.shape_yellow_light_radius5));

                break;

            case R.id.tv_work_tips:
                clickItem();

                binding.tvWorkTips.setBackgroundDrawable(getResources().getDrawable(R.drawable.shape_yellow_light_radius5));
                break;

            case R.id.tv_work_record:
                clickItem();

                binding.tvWorkRecord.setBackgroundDrawable(getResources().getDrawable(R.drawable.shape_yellow_light_radius5));
                break;

            case R.id.tv_equipment_manager:
                clickItem();
                binding.tvEquipmentManager.setBackgroundDrawable(getResources().getDrawable(R.drawable.shape_yellow_light_radius5));
                break;

            case R.id.tv_people_manager:
                clickItem();
                binding.tvPeopleManager.setBackgroundDrawable(getResources().getDrawable(R.drawable.shape_yellow_light_radius5));
                break;

            case R.id.tv_close_screen:

                // 熄灭屏幕
                start();
//                startRecog();

                break;
            case R.id.tv_working_time_title:
                stop();


                //
//                stopRecog();

                break;
        }
    }

    // 清除按钮样式
    private void clickItem() {
        binding.tvWorkHome.setBackgroundColor(getResources().getColor(R.color.transparent));
        binding.tvWorkVideo.setBackgroundColor(getResources().getColor(R.color.transparent));
        binding.tvWorkTips.setBackgroundColor(getResources().getColor(R.color.transparent));
        binding.tvWorkRecord.setBackgroundColor(getResources().getColor(R.color.transparent));
        binding.tvEquipmentManager.setBackgroundColor(getResources().getColor(R.color.transparent));
        binding.tvPeopleManager.setBackgroundColor(getResources().getColor(R.color.transparent));
    }
}
