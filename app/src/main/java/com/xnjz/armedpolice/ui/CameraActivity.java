package com.xnjz.armedpolice.ui;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.hardware.Camera;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.Nullable;

import com.baidu.idl.main.facesdk.model.BDFaceImageInstance;
import com.baidu.idl.main.facesdk.model.BDFaceSDKCommon;
import com.bumptech.glide.Glide;
import com.example.datalibrary.api.FaceApi;
import com.example.datalibrary.callback.CameraDataCallback;
import com.example.datalibrary.callback.FaceDetectCallBack;
import com.example.datalibrary.db.DBManager;
import com.example.datalibrary.gatecamera.CameraPreviewManager;
import com.example.datalibrary.gl.view.GlMantleSurfacView;
import com.example.datalibrary.listener.DBLoadListener;
import com.example.datalibrary.listener.SdkInitListener;
import com.example.datalibrary.manager.FaceSDKManager;
import com.example.datalibrary.model.BDFaceCheckConfig;
import com.example.datalibrary.model.BDFaceImageConfig;
import com.example.datalibrary.model.LivenessModel;
import com.example.datalibrary.model.User;
import com.example.datalibrary.utils.BitmapUtils;
import com.example.datalibrary.utils.FaceOnDrawTexturViewUtil;
import com.example.datalibrary.utils.FileUtils;
import com.google.gson.Gson;
import com.xnjz.armedpolice.MyApp;
import com.xnjz.armedpolice.R;
import com.xnjz.armedpolice.base.BaseActivity;
import com.xnjz.armedpolice.databinding.ActivityMainBinding;
import com.xnjz.armedpolice.demo.SingleBaseConfig;
import com.xnjz.armedpolice.ui.bean.UserInfoBean;
import com.xnjz.armedpolice.ui.model.WorkModel;
import com.xnjz.armedpolice.util.FaceUtils;

import java.io.File;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;
import java.util.UUID;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

public class CameraActivity extends BaseActivity {


    private ActivityMainBinding binding;

    private static final int PREFER_WIDTH = SingleBaseConfig.getBaseConfig().getRgbAndNirWidth();
    private static final int PREFER_HEIGHT = SingleBaseConfig.getBaseConfig().getRgbAndNirHeight();
    private GlMantleSurfacView glMantleSurfacView;

    private BDFaceImageConfig bdFaceImageConfig; //  摄像头逐帧数据
    private BDFaceCheckConfig bdFaceCheckConfig; // 识别阈值


    // 创建注册线程
    private boolean isItemPush;
    private ExecutorService es2 = Executors.newSingleThreadExecutor();
    private Future future2;

    // 识别到的人员信息展示
    private TextView textUser, textSex, textNation, textBirthday, textAddress, textIdCard, textCardUnit, textExpiredDate;

    private ImageView checkTakeImageView, cardImageView;

    // 识别到的用户
    private User user;

    private String userName = "";
    private LinearLayout successLayout;

    private boolean TurnSuccess = true;
    private ImageView sceneTakeImageView;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        requestPermissions(99);

        bdFaceCheckConfig = FaceUtils.getInstance().getBDFaceCheckConfig();

        glMantleSurfacView = findViewById(R.id.face_camera);

        glMantleSurfacView.initSurface(SingleBaseConfig.getBaseConfig().getRgbRevert(),
                SingleBaseConfig.getBaseConfig().getMirrorVideoRGB(), false);


        CameraPreviewManager.getInstance().startPreview(glMantleSurfacView,
                SingleBaseConfig.getBaseConfig().getRgbVideoDirection(),
                PREFER_WIDTH,
                PREFER_HEIGHT);

        initView();

        initListener();

        dbInit();


    }

    @Override
    protected void onResume() {
        super.onResume();
        startTestOpenDebugRegisterFunction();
    }


    @Override
    protected void onPause() {
        super.onPause();
        CameraPreviewManager.getInstance().stopPreview();
    }

    private void initView() {
        textUser = findViewById(R.id.tv_name);
        textSex = findViewById(R.id.tv_sex);
        textNation = findViewById(R.id.tv_nation);
        textBirthday = findViewById(R.id.tv_birthday);
        textAddress = findViewById(R.id.tv_address);
        textIdCard = findViewById(R.id.tv_id_card);
        textCardUnit = findViewById(R.id.tv_id_card_unit);
        textExpiredDate = findViewById(R.id.tv_expired_date);


        checkTakeImageView = findViewById(R.id.iv_check_take);
        cardImageView = findViewById(R.id.iv_card_image);


        successLayout = findViewById(R.id.layout_success);

        sceneTakeImageView = findViewById(R.id.iv_scene_take);
        sceneTakeImageView.setOnClickListener(view -> {

            addUser();
        });

    }


    private void dbInit() {
        // 数据库初始化方法，该方法会返回当前数据库中的所有用户信息
        FaceApi.getInstance().init(new DBLoadListener() {
            @Override
            public void onStart(int successCount) {

            }

            @Override
            public void onLoad(int finishCount, int successCount, float progress) {

            }

            @Override
            public void onComplete(List<User> users, int successCount) {
                // 提取后将数据保存到应用内存中
                FaceApi.getInstance().setUsers(users);
                // 将应用内存中的人脸数据push到sdk内存
                FaceSDKManager.getInstance().initDataBases(CameraActivity.this);
            }

            @Override
            public void onFail(int finishCount, int successCount, List<User> users) {
                FaceApi.getInstance().setUsers(users);
                FaceSDKManager.getInstance().initDataBases(CameraActivity.this);
            }
        }, this);
    }

    private void initListener() {
        if (FaceSDKManager.initStatus != FaceSDKManager.SDK_MODEL_LOAD_SUCCESS) {
            FaceSDKManager.getInstance().initModel(CameraActivity.this, FaceUtils.getInstance().getBDFaceSDKConfig(), new SdkInitListener() {
                @Override
                public void initStart() {

                }

                @Override
                public void initLicenseSuccess() {

                }

                @Override
                public void initLicenseFail(int errorCode, String msg) {

                }

                @Override
                public void initModelSuccess() {
                    FaceSDKManager.initModelSuccess = true;
//                    ToastUtils.toast(CameraActivity.this, "模型加载成功， 欢迎使用");
                }

                @Override
                public void initModelFail(int errorCode, String msg) {
                    FaceSDKManager.initModelSuccess = false;
                }
            });
        }

    }


    @Override
    public View getLayoutBinding() {
        binding = ActivityMainBinding.inflate(getLayoutInflater());
        return binding.getRoot();
    }

    // 开始识别
    private void startTestOpenDebugRegisterFunction() {
        CameraPreviewManager.getInstance().setCameraFacing(0);
        int[] cameraSize = CameraPreviewManager.getInstance().initCamera();

        initFaceConfig(cameraSize[1], cameraSize[0]);
        CameraPreviewManager.getInstance().setmCameraDataCallback(new CameraDataCallback() {

            @Override
            public void onGetCameraData(byte[] data, Camera camera, int width, int height) {
                bdFaceImageConfig.setData(data);
                checkData();
                glMantleSurfacView.onGlDraw(null, null, null);
            }
        });

    }

    // 比对数据
    private void checkData() {
        FaceSDKManager.getInstance().onDetectCheck(bdFaceImageConfig, null, null,
                bdFaceCheckConfig, new FaceDetectCallBack() {
                    @Override
                    public void onFaceDetectCallback(LivenessModel livenessModel) {

//                        User user = livenessModel.getUser();
//                        String faceToken = user.getFaceToken();
//
//                        FaceInfo faceInfo = livenessModel.getFaceInfo();
//                        byte[] feature = livenessModel.getFeature();
//
//                        BDFaceImageInstance bdFaceImageInstance = livenessModel.getBdFaceImageInstance();
//
//                        livenessModel.getAllDetectDuration()
//
//
//                        // rgb回显图赋值显示
//                        BDFaceImageInstance image = livenessModel.getBdFaceImageInstance();
//                        if (image != null) {
//                            testImageview.setImageBitmap(BitmapUtils.getInstaceBmp(image));
//                        }
//
//
//
//                        LogUtils.d("");
                        // 识别结果返回 包含活体得分，质量得分， 识别人员信息， 单接口与整体耗时

                        runOnUiThread(() -> {
                            if (livenessModel == null) {
                                return;
                            }

                            if (livenessModel.getUser() != null) {
                                if (TurnSuccess) {
                                    TurnSuccess = false;
                                } else {
                                    return;
                                }
                                // rgb回显图赋值显示
                                BDFaceImageInstance image = livenessModel.getBdFaceImageInstance();
                                if (image != null) {
                                    sceneTakeImageView.setImageBitmap(BitmapUtils.getInstaceBmp(image));
                                }

                                user = livenessModel.getUser();
                                successLayout.setVisibility(View.VISIBLE);
                                userName = user.getUserName();
                                textUser.setText(user.getUserName());

                                String userInfo = user.getUserInfo();
                                UserInfoBean userInfoBean = new Gson().fromJson(userInfo, UserInfoBean.class);
                                if (userInfoBean != null) {
                                    textSex.setText(userInfoBean.getSex());
                                    textNation.setText(userInfoBean.getNation());
                                    textBirthday.setText(userInfoBean.getBirthday());
                                    textAddress.setText(userInfoBean.getAddressStr());
                                    textCardUnit.setText(userInfoBean.getCardUnit());
                                    textIdCard.setText(userInfoBean.getIdCard());
                                    textExpiredDate.setText(userInfoBean.getExpiredDate());
                                }

                                String imageUrl = getImageUrl(user.getUserName());
                                Glide.with(MyApp.getInstance())
                                        .load(imageUrl)
                                        .into(checkTakeImageView);

                                Glide.with(MyApp.getInstance())
                                        .load(imageUrl)
                                        .into(cardImageView);


                                TimerTask task = new TimerTask() {
                                    @Override
                                    public void run() {
                                        /**
                                         *要执行的操作
                                         */
                                        Intent intent = new Intent(CameraActivity.this, WorkingActivity.class);
                                        intent.putExtra("username", userName);
                                        startActivity(intent);
                                        finish();
                                    }
                                };
                                Timer timer = new Timer();
                                timer.schedule(task, 2000);
                            } else {
                                user = null;
//                                textUser.setText("未检测到人脸");
                            }
                        });
                    }

                    @Override
                    public void onTip(int code, String msg) {

                    }

                    @Override
                    public void onFaceDetectDarwCallback(LivenessModel livenessModel) {

                        if (livenessModel == null) {
                            return;
                        }

                        glMantleSurfacView.onGlDraw(livenessModel.getTrackFaceInfo(),
                                livenessModel.getBdFaceImageInstance(),
                                FaceOnDrawTexturViewUtil.drawFaceColor(user, livenessModel));
                    }
                });
    }


    // 初始化人脸图片配置
    private void initFaceConfig(int height, int width) {
        bdFaceImageConfig = new BDFaceImageConfig(height, width,
                SingleBaseConfig.getBaseConfig().getRgbDetectDirection(),
                SingleBaseConfig.getBaseConfig().getMirrorDetectRGB(),
                BDFaceSDKCommon.BDFaceImageType.BDFACE_IMAGE_TYPE_YUV_NV21);
    }


    // 注册逻辑
    private void addUser() {
        if (isItemPush) {
            return;
        }

        isItemPush = true;

        if (future2 != null && !future2.isDone()) {
            return;
        }

        future2 = es2.submit(() -> {
            addFeature();
            isItemPush = false;
        });
    }

    private void addFeature() {
        // sd卡根目录下的Face-Import目录，在该目录下放入人脸照片
        File batchImportDir = FileUtils.getBatchImportDirectory();

        File[] files = batchImportDir.listFiles();

        for (int i = 0; i < files.length; i++) {
            File file = files[i];

            // 提取人脸名称
            String picName = file.getName();
            String userName = FileUtils.getFileNameNoEx(picName);

            // 在数据库中查询该人员是否存在，若存在先删除该人员
            List<User> listUsers = FaceApi.getInstance().getUserListByUserName(userName);
            if (listUsers != null && listUsers.size() > 0) {
                FaceApi.getInstance().userDelete(listUsers.get(0).getUserId());

                synchronized (FaceSDKManager.getInstance().getFaceSearch()) {
                    FaceSDKManager.getInstance().getFaceSearch().delPersonById(listUsers.get(0).getId());
                }
            }

            // 提取目录下的人脸图片特征，
            // bytes为特征值，
            // ret为提取状态
            // 128 为成功
            final Bitmap bitmap = BitmapFactory.decodeFile(file.getAbsolutePath());
            byte[] bytes = new byte[512];
            float ret = FaceSDKManager.getInstance().personDetect(bitmap, bytes, null, CameraActivity.this);

            if (bitmap != null && !bitmap.isRecycled()) {
                bitmap.recycle();
            }

            // 如果成功，进行人脸保存
            if (ret == 128) {
                // 将人员信息保存到数据库
                User user = new User();
                user.setGroupId(DBManager.GROUP_ID);

                // todo  用户id 有数字 字母 下划线组成长度限制128 B
                // todo  uid 为用户的id 百度对uid 不做限制和处理 ， 应该和您的账号系统中的用户id 对应
                String uid = UUID.randomUUID().toString();


                UserInfoBean userInfoBean = getUserInfoBean(userName);
                if (userInfoBean == null) {
                    user.setUserId(uid);
                } else {
                    user.setUserId(userInfoBean.getId());
                    user.setUserInfo(new Gson().toJson(userInfoBean));
                }
                user.setUserName(userName);
                user.setFeature(bytes);
                user.setImageName(picName);
                boolean importDBSuccess = FaceApi.getInstance().userAdd(user);

                if (importDBSuccess) {
                    listUsers = FaceApi.getInstance().getUserListByUserName(userName);

                    user = listUsers == null || listUsers.size() == 0 ? null : listUsers.get(0);
                    synchronized (FaceSDKManager.getInstance().getFaceSearch()) {
                        FaceSDKManager.getInstance().getFaceSearch().pushPersonById(user.getId(), user.getFeature());
                    }
                }
            }

        }
    }

    private UserInfoBean getUserInfoBean(String userName) {
        WorkModel workModel = new WorkModel();
        List<UserInfoBean> userInfo = workModel.getUserInfo();
        for (UserInfoBean userInfoBean : userInfo) {
            if (TextUtils.equals(userInfoBean.getName(), userName)) {
                return userInfoBean;
            }
        }
        return null;
    }

    /**
     * 获取本地图片url
     *
     * @param userName
     * @return
     */
    private String getImageUrl(String userName) {
        // sd卡根目录下的Face-Import目录，在该目录下放入人脸照片
        File batchImportDir = FileUtils.getBatchImportDirectory();

        File[] files = batchImportDir.listFiles();

        if (files != null) {
            for (int i = 0; i < files.length; i++) {
                File file = files[i];
                // 提取人脸名称
                String picName = file.getName();
                String userNamePic = FileUtils.getFileNameNoEx(picName);
                if (TextUtils.equals(userNamePic, userName)) {
                    return file.getAbsolutePath();
                }
            }
        }
        return "";
    }

}
