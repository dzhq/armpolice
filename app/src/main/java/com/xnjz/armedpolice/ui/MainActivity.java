package com.xnjz.armedpolice.ui;


import android.content.Context;
import android.content.Intent;
import android.hardware.Camera;
import android.os.Bundle;
import android.view.View;

import com.baidu.idl.main.facesdk.identifylibrary.utils.FaceUtils;
import com.baidu.idl.main.facesdk.model.BDFaceSDKCommon;
import com.example.datalibrary.callback.CameraDataCallback;
import com.example.datalibrary.callback.FaceDetectCallBack;
import com.example.datalibrary.gatecamera.CameraPreviewManager;
import com.example.datalibrary.listener.SdkInitListener;
import com.example.datalibrary.manager.FaceSDKManager;
import com.example.datalibrary.manager.SaveImageManager;
import com.example.datalibrary.model.BDFaceCheckConfig;
import com.example.datalibrary.model.BDFaceImageConfig;
import com.example.datalibrary.model.BDLiveConfig;
import com.example.datalibrary.model.LivenessModel;
import com.example.datalibrary.model.User;
import com.example.datalibrary.utils.FaceOnDrawTexturViewUtil;
import com.example.datalibrary.utils.ToastUtils;
import com.xnjz.armedpolice.base.BaseActivity;
import com.xnjz.armedpolice.databinding.ActivityMainBinding;
import com.xnjz.armedpolice.demo.SingleBaseConfig;
import com.xnjz.armedpolice.ui.WorkingActivity;


public class MainActivity extends BaseActivity {

    private ActivityMainBinding binding;


    private User user;

    private Context mContext;
    private boolean isSaveImage;

    private static final int PREFER_WIDTH = SingleBaseConfig.getBaseConfig().getRgbAndNirWidth();
    private static final int PREFER_HEIGHT = SingleBaseConfig.getBaseConfig().getRgbAndNirHeight();
    private BDFaceImageConfig bdFaceImageConfig;
    private BDFaceCheckConfig bdFaceCheckConfig;
    private BDLiveConfig bdLiveConfig;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mContext = this;
        setContentView(getLayoutBinding());
        initFaceCheck();
        initView();

        initLicense();
    }

    private void initFaceCheck() {
        bdFaceCheckConfig = FaceUtils.getInstance().getBDFaceCheckConfig();
        bdLiveConfig = FaceUtils.getInstance().getBDLiveConfig();
    }

    private void initLicense() {
        if (FaceSDKManager.initStatus != FaceSDKManager.SDK_MODEL_LOAD_SUCCESS) {
            FaceSDKManager.getInstance().initModel(mContext,
                    FaceUtils.getInstance().getBDFaceSDKConfig() , new SdkInitListener() {
                        @Override
                        public void initStart() {
                        }

                        @Override
                        public void initLicenseSuccess() {
                        }

                        @Override
                        public void initLicenseFail(int errorCode, String msg) {
                        }

                        @Override
                        public void initModelSuccess() {
                            FaceSDKManager.initModelSuccess = true;
                            ToastUtils.toast(mContext, "模型加载成功，欢迎使用");
                        }

                        @Override
                        public void initModelFail(int errorCode, String msg) {
                            FaceSDKManager.initModelSuccess = false;
                            if (errorCode != -12) {
                                ToastUtils.toast(mContext, "模型加载失败，请尝试重启应用");
                            }
                        }
                    });
        }
    }


//    private void initRGBCheck(){
//        if (isSetCameraId()){
//            return;
//        }
//        int mCameraNum = Camera.getNumberOfCameras();
//        Camera.CameraInfo cameraInfo = new Camera.CameraInfo();
//        for (int i = 0; i < Camera.getNumberOfCameras(); i++) {
//            Camera.getCameraInfo(i, cameraInfo);
//        }
//        if (mCameraNum > 1){
//            try {
//                mCamera = new Camera[mCameraNum];
//                previewTextures = new PreviewTexture[mCameraNum];
//                mCamera[0] = Camera.open(0);
//                previewTextures[0] = new PreviewTexture(this, checkRBGTexture);
//                previewTextures[0].setCamera(mCamera[0], PREFER_WIDTH, PREFER_HEIGHT);
//                mCamera[0].setPreviewCallback(new Camera.PreviewCallback() {
//                    @Override
//                    public void onPreviewFrame(byte[] data, Camera camera) {
//                        int check = StreamUtil.checkNirRgb(data, PREFER_WIDTH, PREFER_HEIGHT);
//                        if (check == 1){
//                            setRgbCameraId(0);
//                        }
//                        release(0);
//                    }
//                });
//            }catch (Exception e){
//                e.printStackTrace();
//            }
//            try {
//                mCamera[1] = Camera.open(1);
//                previewTextures[1] = new PreviewTexture(this, checkNIRTexture);
//                previewTextures[1].setCamera(mCamera[1], PREFER_WIDTH, PREFER_HEIGHT);
//                mCamera[1].setPreviewCallback(new Camera.PreviewCallback() {
//                    @Override
//                    public void onPreviewFrame(byte[] data, Camera camera) {
//                        int check = StreamUtil.checkNirRgb(data, PREFER_WIDTH, PREFER_HEIGHT);
//                        if (check == 1){
//                            setRgbCameraId(1);
//                        }
//                        release(1);
//                    }
//                });
//            }catch (Exception e){
//                e.printStackTrace();
//            }
//        } else {
//            setRgbCameraId(0);
//        }
//    }

    private void initView() {

        binding.layoutInfoBottom.setOnClickListener(view -> {
            startActivity(new Intent(this, WorkingActivity.class));
        });


        binding.faceCamera.initSurface(SingleBaseConfig.getBaseConfig().getRgbRevert(),
                SingleBaseConfig.getBaseConfig().getMirrorVideoRGB() , com.baidu.idl.main.facesdk.identifylibrary.model.SingleBaseConfig.getBaseConfig().isOpenGl());
        CameraPreviewManager.getInstance().startPreview(/*mContext, */binding.faceCamera,
               SingleBaseConfig.getBaseConfig().getRgbVideoDirection() , PREFER_WIDTH, PREFER_HEIGHT);


        bdFaceCheckConfig = FaceUtils.getInstance().getBDFaceCheckConfig();
    }

    @Override
    protected void onResume() {
        super.onResume();
        startCameraPreview();
    }

    private void startCameraPreview() {
        // 设置前置摄像头
         CameraPreviewManager.getInstance().setCameraFacing(CameraPreviewManager.CAMERA_FACING_FRONT);
        // 设置后置摄像头
        // CameraPreviewManager.getInstance().setCameraFacing(CameraPreviewManager.CAMERA_FACING_BACK);
        // 设置USB摄像头
        // TODO ： 临时放置
        // CameraPreviewManager.getInstance().setCameraFacing(CameraPreviewManager.CAMERA_USB);
        if (SingleBaseConfig.getBaseConfig().getRBGCameraId() != -1) {
            CameraPreviewManager.getInstance().setCameraFacing(SingleBaseConfig.getBaseConfig().getRBGCameraId());
        } else {
            CameraPreviewManager.getInstance().setCameraFacing(CameraPreviewManager.CAMERA_USB);
        }
        int[] cameraSize =  CameraPreviewManager.getInstance().initCamera();
        initFaceConfig(cameraSize[1] , cameraSize[0]);
        isPause = true;
        CameraPreviewManager.getInstance().setmCameraDataCallback(new CameraDataCallback() {
            @Override
            public void onGetCameraData(byte[] data, Camera camera, int width, int height) {
                bdFaceImageConfig.setData(data);
                // 预览模式或者开发模式上传图片成功开始
                if (bdFaceCheckConfig.getSecondFeature() != null) {
                    binding.faceCamera.setFrame();
                    // rgb回显图显示
//                    testImageview.setVisibility(View.VISIBLE);
                    // 拿到相机帧数据
                    // 摄像头预览数据进行人脸检测
                    FaceSDKManager.getInstance().onDetectCheck(bdFaceImageConfig, null, null,
                            bdFaceCheckConfig, new FaceDetectCallBack() {
                                @Override
                                public void onFaceDetectCallback(final LivenessModel livenessModel) {
                                    // 预览模式
                                    checkCloseDebugResult(livenessModel);
                                    // 开发模式
                                    checkOpenDebugResult(livenessModel);
                                    if (isSaveImage){
                                        SaveImageManager.getInstance().saveImage(livenessModel , bdLiveConfig);
                                    }
                                }

                                @Override
                                public void onTip(int code, final String msg) {

                                }

                                @Override
                                public void onFaceDetectDarwCallback(LivenessModel livenessModel) {
                                    // 人脸框显示
                                    showFrame(livenessModel);
                                }
                            });


                } else {
                    binding.faceCamera.onGlDraw();
                    // 如果开发模式或者预览模式没上传图片则显示蒙层
//                    testImageview.setImageResource(com.baidu.idl.main.facesdk.identifylibrary.R.mipmap.ic_image_video);
//                    ObjectAnimator animator = ObjectAnimator.ofFloat(view, "alpha", 0.85f, 0.0f);
//                    animator.setDuration(3000);
//                    view.setBackgroundColor(Color.parseColor("#ffffff"));
//                    animator.start();
                }
            }
        });

    }


    // 开发模式
    private void checkOpenDebugResult(final LivenessModel model) {
//        runOnUiThread(new Runnable() {
//            @Override
//            public void run() {
//                if (model == null) {
//                    // 提示隐藏
//                    layoutCompareStatus.setVisibility(View.GONE);
//                    // 阈值
//                    personKaifaIv.setVisibility(View.GONE);
//                    // 显示默认图片
//                    testImageview.setImageResource(com.baidu.idl.main.facesdk.identifylibrary.R.mipmap.ic_image_video);
//                    // 默认值为0
//                    tvRgbLiveTime.setText(String.format("相似度分数：%s", 0));
//                    tvRgbLiveScore.setText(String.format("活体检测耗时：%s ms", 0));
//                    tvFeatureTime.setText(String.format("特征抽取耗时：%s ms", 0));
//                    tvFeatureSearchTime.setText(String.format("特征比对耗时：%s ms", 0));
//                    tvAllTime.setText(String.format("总耗时：%s ms", 0));
//                } else {
//                    // rgb回显图赋值显示
//                    BDFaceImageInstance image = model.getBdFaceImageInstance();
//                    if (image != null) {
//                        testImageview.setImageBitmap(BitmapUtils.getInstaceBmp(image));
//                    }
//                    tvRgbLiveTime.setText(String.format("相似度分数：%s", score));
//                    tvRgbLiveScore.setText(String.format("活体检测耗时：%s ms", model.getRgbLivenessDuration()));
//                    tvFeatureTime.setText(String.format("特征抽取耗时：%s ms", model.getFeatureDuration()));
//                    tvFeatureSearchTime.setText(String.format("特征比对耗时：%s ms", model.getCheckDuration()));
//
//
//                    if (isDevelopment) {
//                        testimonyTipsFailRl.setVisibility(View.GONE);
//                        layoutCompareStatus.setVisibility(View.VISIBLE);
//                        if (model .isQualityCheck()) {
//                            tvFeatureTime.setText(String.format("特征抽取耗时：%s ms", 0));
//                            tvFeatureSearchTime.setText(String.format("特征比对耗时：%s ms", 0));
//                            long l = model.getRgbDetectDuration() + model.getRgbLivenessDuration();
//                            tvAllTime.setText(String.format("总耗时：%s ms", l));
//                            personKaifaIv.setVisibility(View.VISIBLE);
//                            personKaifaIv.setImageResource(com.baidu.idl.main.facesdk.identifylibrary.R.mipmap.ic_icon_develop_fail);
//                            textCompareStatus.setTextColor(Color.parseColor("#FECD33"));
//                            /*textCompareStatus.setMaxEms(6)*/;
//                            textCompareStatus.setText("请正视摄像头");
//                        } else if (isFace == true) {
//                            textCompareStatus.setTextColor(Color.parseColor("#FECD33"));
//                            textCompareStatus.setText("比对失败");
//                        } else {
//                            if (mLiveType == 0) {
//                                tvAllTime.setText(String.format("总耗时：%s ms", model.getAllDetectDuration()));
//                                if (score > com.baidu.idl.main.facesdk.identifylibrary.model.SingleBaseConfig.getBaseConfig().getIdThreshold()) {
//                                    textCompareStatus.setTextColor(Color.parseColor("#00BAF2"));
//                                    textCompareStatus.setText("比对成功");
//                                } else {
//                                    textCompareStatus.setTextColor(Color.parseColor("#FECD33"));
//                                    textCompareStatus.setText("比对失败");
//                                }
//                            } else {
//                                // 活体阈值判断显示
//                                rgbLivenessScore = model.getRgbLivenessScore();
//                                if (rgbLivenessScore < mRgbLiveScore) {
//                                    personKaifaIv.setVisibility(View.VISIBLE);
//                                    personKaifaIv.setImageResource(com.baidu.idl.main.facesdk.identifylibrary.R.mipmap.ic_icon_develop_fail);
//                                    textCompareStatus.setTextColor(Color.parseColor("#FECD33"));
//
////                            textCompareStatus.setMaxEms(7);
//                                    textCompareStatus.setText("活体检测未通过");
//                                } else {
//                                    personKaifaIv.setVisibility(View.VISIBLE);
//                                    personKaifaIv.setImageResource(com.baidu.idl.main.facesdk.identifylibrary.R.mipmap.ic_icon_develop_success);
//                                    if (score > com.baidu.idl.main.facesdk.identifylibrary.model.SingleBaseConfig.getBaseConfig().getIdThreshold()) {
//                                        textCompareStatus.setTextColor(Color.parseColor("#00BAF2"));
//                                        textCompareStatus.setText("比对成功");
//                                    } else {
//                                        textCompareStatus.setTextColor(Color.parseColor("#FECD33"));
//                                        textCompareStatus.setText("比对失败");
//                                    }
//                                }
//                                tvFeatureTime.setText(String.format("特征抽取耗时：%s ms", model.getFeatureDuration()));
//                                tvFeatureSearchTime.setText(String.format("特征比对耗时：%s ms",
//                                        model.getCheckDuration()));
//
//                                tvAllTime.setText(String.format("总耗时：%s ms", model.getAllDetectDuration()));
//                            }
//                        }
//                    }
//                }
//            }
//        });
    }



    // 预览模式
    private void checkCloseDebugResult(final LivenessModel model) {
        // 当未检测到人脸UI显示
//        runOnUiThread(new Runnable() {
//            @Override
//            public void run() {
//                if (model == null) {
//                    // 提示隐藏
//                    testimonyTipsFailRl.setVisibility(View.GONE);
//                    if (testimonyPreviewLineIv.getVisibility() == View.VISIBLE) {
//                        ObjectAnimator animator = ObjectAnimator.ofFloat(view, "alpha", 0.85f, 0.0f);
//                        animator.setDuration(3000);
//                        animator.addListener(new AnimatorListenerAdapter() {
//                            @Override
//                            public void onAnimationEnd(Animator animation) {
//                                super.onAnimationEnd(animation);
//                            }
//
//                            @Override
//                            public void onAnimationStart(Animator animation) {
//                                super.onAnimationStart(animation);
//                                view.setBackgroundColor(Color.parseColor("#ffffff"));
//                            }
//                        });
//                        animator.start();
//                    }
//                } else {
//
//                    runOnUiThread(new Runnable() {
//                        @Override
//                        public void run() {
//                            score = model.getScore();
//
//                            if (isDevelopment == false) {
//                                layoutCompareStatus.setVisibility(View.GONE);
//                                testimonyTipsFailRl.setVisibility(View.VISIBLE);
//                                if (isFace == true) {
//                                    testimonyTipsFailTv.setText("上传图片不包含人脸");
//                                    testimonyTipsFailTv.setTextColor(Color.parseColor("#FFFEC133"));
//                                    testimonyTipsPleaseFailTv.setText("无法进行人证比对");
//                                    testimonyTipsFailIv.setImageResource(com.baidu.idl.main.facesdk.identifylibrary.R.mipmap.tips_fail);
//                                } else {
//                                    if (mLiveType == 0) {
//                                        if (score > com.baidu.idl.main.facesdk.identifylibrary.model.SingleBaseConfig.getBaseConfig().getIdThreshold()) {
//                                            testimonyTipsFailTv.setText("人证核验通过");
//                                            testimonyTipsFailTv.setTextColor(
//                                                    Color.parseColor("#FF00BAF2"));
//                                            testimonyTipsPleaseFailTv.setText("识别成功");
//                                            testimonyTipsFailIv.setImageResource(com.baidu.idl.main.facesdk.identifylibrary.R.mipmap.tips_success);
//                                        } else {
//                                            testimonyTipsFailTv.setText("人证核验未通过");
//                                            testimonyTipsFailTv.setTextColor(
//                                                    Color.parseColor("#FFFEC133"));
//                                            testimonyTipsPleaseFailTv.setText("请上传正面人脸照片");
//                                            testimonyTipsFailIv.setImageResource(com.baidu.idl.main.facesdk.identifylibrary.R.mipmap.tips_fail);
//                                        }
//                                    } else {
//                                        // 活体阈值判断显示
//                                        rgbLivenessScore = model.getRgbLivenessScore();
//                                        if (rgbLivenessScore < mRgbLiveScore) {
//                                            testimonyTipsFailTv.setText("人证核验未通过");
//                                            testimonyTipsFailTv.setTextColor(
//                                                    Color.parseColor("#FFFEC133"));
//                                            testimonyTipsPleaseFailTv.setText("请上传正面人脸照片");
//                                            testimonyTipsFailIv.setImageResource(com.baidu.idl.main.facesdk.identifylibrary.R.mipmap.tips_fail);
//                                        } else {
//                                            if (score > com.baidu.idl.main.facesdk.identifylibrary.model.SingleBaseConfig.getBaseConfig()
//                                                    .getIdThreshold()) {
//                                                testimonyTipsFailTv.setText("人证核验通过");
//                                                testimonyTipsFailTv.setTextColor(
//                                                        Color.parseColor("#FF00BAF2"));
//                                                testimonyTipsPleaseFailTv.setText("识别成功");
//                                                testimonyTipsFailIv.setImageResource(
//                                                        com.baidu.idl.main.facesdk.identifylibrary.R.mipmap.tips_success);
//                                            } else {
//                                                testimonyTipsFailTv.setText("人证核验未通过");
//                                                testimonyTipsFailTv.setTextColor(
//                                                        Color.parseColor("#FFFEC133"));
//                                                testimonyTipsPleaseFailTv.setText("请上传正面人脸照片");
//                                                testimonyTipsFailIv.setImageResource(
//                                                        com.baidu.idl.main.facesdk.identifylibrary.R.mipmap.tips_fail);
//                                            }
//                                        }
//                                    }
//                                }
//                            }
//                        }
//                    });
//
//
//                }
//            }
//        });
    }


    private boolean isPause = false;
    public void showFrame(LivenessModel livenessModel){
        if (livenessModel == null){
            return;
        }

        if (isPause){
//            binding.faceCamera.onGlDraw(livenessModel.getTrackFaceInfo() ,
//                    livenessModel.getBdFaceImageInstance() ,
//                    FaceOnDrawTexturViewUtil.drawFaceColor(score > FaceUtils.getInstance().getThreshold()));
        }
    }


    private void checkData() {
        FaceSDKManager.getInstance().onDetectCheck(bdFaceImageConfig , null, null, bdFaceCheckConfig, new FaceDetectCallBack(){

            @Override
            public void onFaceDetectCallback(LivenessModel livenessModel) {

            }

            @Override
            public void onTip(int code, String msg) {

            }

            @Override
            public void onFaceDetectDarwCallback(LivenessModel livenessModel) {

                if(livenessModel  == null) return;

                binding.faceCamera.onGlDraw(livenessModel.getTrackFaceInfo(), livenessModel.getBdFaceImageInstance(),
                        FaceOnDrawTexturViewUtil.drawFaceColor(user, livenessModel));
            }
        });

    }

    @Override
    public View getLayoutBinding() {
        binding = ActivityMainBinding.inflate(getLayoutInflater());
        return binding.getRoot();
    }



//    private void startCameraPreview() {
//        // 设置前置摄像头
//        // CameraPreviewManager.getInstance().setCameraFacing(CameraPreviewManager.CAMERA_FACING_FRONT);
//        // 设置后置摄像头
//        // CameraPreviewManager.getInstance().setCameraFacing(CameraPreviewManager.CAMERA_FACING_BACK);
//        // 设置USB摄像头
//        // TODO ： 临时放置
//        // CameraPreviewManager.getInstance().setCameraFacing(CameraPreviewManager.CAMERA_USB);
//        if (SingleBaseConfig.getBaseConfig().getRBGCameraId() != -1) {
//            CameraPreviewManager.getInstance().setCameraFacing(SingleBaseConfig.getBaseConfig().getRBGCameraId());
//        } else {
//            CameraPreviewManager.getInstance().setCameraFacing(CameraPreviewManager.CAMERA_USB);
//        }
//        int[] cameraSize =  CameraPreviewManager.getInstance().initCamera();
//        initFaceConfig(cameraSize[1] , cameraSize[0]);
//        isPause = true;
//        CameraPreviewManager.getInstance().setmCameraDataCallback(new CameraDataCallback() {
//            @Override
//            public void onGetCameraData(byte[] data, Camera camera, int width, int height) {
//                bdFaceImageConfig.setData(data);
//                // 预览模式或者开发模式上传图片成功开始
//                if (bdFaceCheckConfig.getSecondFeature() != null) {
//                    glSurfaceView.setFrame();
//                    // rgb回显图显示
////                    testImageview.setVisibility(View.VISIBLE);
//                    // 拿到相机帧数据
//                    // 摄像头预览数据进行人脸检测
//                    com.example.datalibrary.manager.FaceSDKManager.getInstance().onDetectCheck(bdFaceImageConfig, null, null,
//                            bdFaceCheckConfig, new FaceDetectCallBack() {
//                                @Override
//                                public void onFaceDetectCallback(final LivenessModel livenessModel) {
//                                    // 预览模式
//                                    checkCloseDebugResult(livenessModel);
//                                    // 开发模式
//                                    checkOpenDebugResult(livenessModel);
//                                    if (isSaveImage){
//                                        SaveImageManager.getInstance().saveImage(livenessModel , bdLiveConfig);
//                                    }
//                                }
//
//                                @Override
//                                public void onTip(int code, final String msg) {
//
//                                }
//
//                                @Override
//                                public void onFaceDetectDarwCallback(LivenessModel livenessModel) {
//                                    // 人脸框显示
//                                    showFrame(livenessModel);
//                                }
//                            });
//
//
//                } else {
//                    glSurfaceView.onGlDraw();
//                    // 如果开发模式或者预览模式没上传图片则显示蒙层
//                    testImageview.setImageResource(R.mipmap.ic_image_video);
//                    ObjectAnimator animator = ObjectAnimator.ofFloat(view, "alpha", 0.85f, 0.0f);
//                    animator.setDuration(3000);
//                    view.setBackgroundColor(Color.parseColor("#ffffff"));
//                    animator.start();
//                }
//            }
//        });
//    }

    private void initFaceConfig(int height , int width){
        bdFaceImageConfig = new BDFaceImageConfig(height , width ,
                SingleBaseConfig.getBaseConfig().getRgbDetectDirection(),
                SingleBaseConfig.getBaseConfig().getMirrorDetectRGB() ,
                BDFaceSDKCommon.BDFaceImageType.BDFACE_IMAGE_TYPE_YUV_NV21);
    }
}